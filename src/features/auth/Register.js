import Navigation from "../accessories/Navigation"
import React, {useEffect, useState} from "react";
import {Col, Container, Alert, Form} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import {useDispatch, useSelector} from "react-redux";
import {register} from "./authSlice";

const registerSelector = (state) => state.auth.registerStatus

function Register() {
    const [username, setUsername] = useState(null)
    const [password, setPassword] = useState(null)
    const [email, setEmail] = useState(null)
    const [repassword, setRepassword] = useState(null)
    const [variant, setVariant] = useState("")
    const [errors, setErrors] = useState([])
    const dispatch = useDispatch()
    let status = useSelector(registerSelector)

    useEffect(() => {
        if (status === 400) {
            setErrors(["User with such email does already exist"])
            setVariant("danger")
        } else if (status === 201) {
            setErrors(["Registered successfully"])
            setVariant("success")
        } else if (status !== undefined) {
            setErrors(["Internal server error, try again later"])
            setVariant("danger")
        }
    }, [status])

    const handleSubmit = () => {
        if (username !== null && password !== null && repassword !== null && email !== null && password === repassword) {
            dispatch(register({username: username, email: email, password: password}))
        } else {
            let errors = []
            if (username === null) {
                errors.push("Name is required")
            }
            if (email === null) {
                errors.push("Email is required")
            }
            if (password === null) {
                errors.push("Position is required")
            }
            if (repassword === null) {
                errors.push("Password confirmation is required")
            }
            if (password !== repassword) {
                errors.push("Password and its confirmation differ")
            }
            setVariant("danger")
            setErrors(errors)
        }
    }

    const listErrors = () => <div className="error-wrapper">{errors.map((error) => {
        return <div>{error}</div>
    })}</div>

    return (
        <Container>
            <Col md={{span: 8, offset: 2}} lg={{span: 6, offset: 3}}
                 style={{justifyContent: "center", textAlign: "center"}}>
                <Navigation view={"register"}/>
                <Form style={{textAlign: "left"}}>
                    <Form.Group controlId="formUsername">
                        <Form.Label style={{margin: "10px 0"}}>Username</Form.Label>
                        <Form.Control type="text" placeholder="Enter username"
                                      onChange={(e) => setUsername(e.target.value)}/>
                    </Form.Group>
                    <Form.Group controlId="formEmail">
                        <Form.Label style={{margin: "10px 0"}}>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email"
                                      onChange={(e) => setEmail(e.target.value)}/>
                    </Form.Group>
                    <Form.Group controlId="formPassword">
                        <Form.Label style={{margin: "10px 0"}}>Password</Form.Label>
                        <Form.Control type="password" rows={4} placeholder="Enter password"
                                      onChange={(e) => setPassword(e.target.value)}/>
                    </Form.Group>
                    <Form.Group controlId="formRepassword">
                        <Form.Label style={{margin: "10px 0"}}>Confirm password</Form.Label>
                        <Form.Control type="password" rows={4} placeholder="Enter password again"
                                      onChange={(e) => setRepassword(e.target.value)}/>
                    </Form.Group>
                </Form>
                {errors.length === 0 ? null :
                    <div id={"error-wrapper"}>
                        <Alert variant={variant} onClose={() => {
                            setErrors([])
                        }} dismissible style={{margin: "20px 0 0 0"}}>
                            {listErrors()}
                        </Alert>
                    </div>
                }
                <Button style={{margin: "20px 0 0 0"}} onClick={handleSubmit}>Submit</Button>
            </Col>
        </Container>
    )
}

export default Register