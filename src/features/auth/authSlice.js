import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

const backend = "http://localhost:5050"

export const login = createAsyncThunk('auth/login', async (cred, thunkAPI) => {
    return fetch(backend + "/auth/login", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(cred)
    }).then(response => {
        if (response.status !== 200) {
            throw thunkAPI.rejectWithValue(response.status)
        } else {
            return response.json()
        }
    }).then(json => {
        localStorage.setItem('username', json.username)
        localStorage.setItem('authHeader', 'Bearer ' + json.accessToken)
        return json
    }).catch(err => {
        return err
    })
})

export const register = createAsyncThunk('auth/register', async (cred, thunkAPI) => {
    return fetch(backend + "/auth/register", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(cred)
    }).then(response => {
        if (response.status !== 201) {
            return thunkAPI.rejectWithValue(response.status)
        } else {
            return response.status
        }
    })
})

const authSlice = createSlice({
    name: 'auth',
    initialState: {},
    reducers: {
        resetLoginStatus(state, action) {
            return {...state, loginStatus: undefined}
        }
    },
    extraReducers: builder => {
        builder.addCase(login.fulfilled, (state, action) => {
            return {...state, loginStatus: 200}
        }).addCase(login.rejected, (state, action) => {
            return {...state, loginStatus: action.payload}
        }).addCase(register.fulfilled, (state, action) => {
            return {...state, registerStatus: action.payload}
        }).addCase(register.rejected, (state, action) => {
            return {...state, registerStatus: action.payload}
        })
    }
})
export const {resetLoginStatus} = authSlice.actions

export default authSlice.reducer