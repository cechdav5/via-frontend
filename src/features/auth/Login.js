import Navigation from "../accessories/Navigation"
import React, {useEffect, useState} from "react";
import {Container, Form, Col, Alert} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import {login, resetLoginStatus} from "../auth/authSlice";
import {useDispatch, useSelector} from "react-redux";


const loginSelector = (state) => state.auth.loginStatus

function Login() {
    const [email, setEmail] = useState(null)
    const [password, setPassword] = useState(null)
    const [errors, setErrors] = useState([])
    const [variant, setVariant] = useState("")
    const dispatch = useDispatch()
    const status = useSelector(loginSelector)

    useEffect(() => {
        if (status === 401) {
            setErrors(["Invalid credentials"])
            setVariant("danger")
        } else if (status === 400) {
            setErrors(["User with such email doesn't exist"])
            setVariant("danger")
        } else if (status === 200) {
            setErrors(["Login successful"])
            setVariant("success")
        } else if (status !== undefined) {
            setErrors(["Internal server error, try again later"])
            setVariant("danger")
        }
    }, [status])

    const handleSubmit = () => {
        if (email !== null && password !== null) {
            dispatch(login({email: email, password: password}))
            setErrors([])
        } else {
            let errors = []
            if (email === null) {
                errors.push("Email is required")
            }
            if (password === null) {
                errors.push("Password is required")
            }
            setVariant("danger")
            setErrors(errors)
        }
    }

    const listErrors = () => <div id="error-wrapper" className="error-wrapper">{errors.map((error) => {
        return <div>{error}</div>
    })}</div>

    return (
        <Container>
            {localStorage.getItem("username") === null ?
                <Col md={{span: 8, offset: 2}} lg={{span: 6, offset: 3}}
                     style={{justifyContent: "center", textAlign: "center"}}>
                    <Navigation view={"login"}/>
                    <Form style={{textAlign: "left"}}>
                        <Form.Group controlId="formEmail">
                            <Form.Label style={{margin: "10px 0"}}>Email</Form.Label>
                            <Form.Control type="text" placeholder="Enter email"
                                          onChange={(e) => setEmail(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="formPassword">
                            <Form.Label style={{margin: "10px 0"}}>Password</Form.Label>
                            <Form.Control type="password" rows={4} placeholder="Enter password"
                                          onChange={(e) => setPassword(e.target.value)}/>
                        </Form.Group>
                    </Form>
                    {errors.length === 0 ? null :
                        <Alert variant={variant} onClose={() => {
                            setErrors([])
                        }} dismissible style={{margin: "20px 0 0 0"}}>
                            {listErrors()}
                        </Alert>
                    }
                    <Button style={{margin: "20px 0 0 0"}} onClick={handleSubmit}>Submit</Button>
                </Col>
                : <Col md={{span: 8, offset: 2}} lg={{span: 6, offset: 3}}
                       style={{justifyContent: "left", textAlign: "left"}}>
                    <Navigation view={"login"}/>
                    <div style={{margin: "20px 0 0 0"}} id={"logged-in-as"}>
                        Logged in as {localStorage.getItem("username")}
                    </div>
                    <Button style={{margin: "20px 0 0 0"}} onClick={() => {
                        localStorage.removeItem("username")
                        localStorage.removeItem("authHeader")
                        dispatch(resetLoginStatus())
                        console.log(status)
                        setErrors([])
                    }}>Log Out</Button>
                </Col>}
        </Container>
    )
}

export default Login