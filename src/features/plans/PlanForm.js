import React, {useEffect, useState} from 'react';
import {LocalizationProvider} from "@mui/x-date-pickers/LocalizationProvider";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {DateTimePicker} from "@mui/x-date-pickers";
import TextField from "@mui/material/TextField";
import dayjs from "dayjs";
import AdvancedFormat from "dayjs/plugin/advancedFormat";
import MapWrapper from "./MapWrapper";
import {useDispatch, useSelector} from "react-redux";
import {Container, Col, Form, Alert} from "react-bootstrap";
import '../../styles/datetimepicker.css';
import Navigation from "../accessories/Navigation"
import Button from "react-bootstrap/Button";
import {createPlan, setCreateStatusInvalid, cleanCreateStatus} from "./plansSlice";
import {resetLoginStatus} from "../auth/authSlice";


const selectCreateStatus = state => state.plans.createStatus

function PlanForm() {

    const [startDate, setStartDate] = useState(null)
    const [endDate, setEndDate] = useState(null)
    const [name, setName] = useState("")
    const [attendees, setAttendees] = useState(0)
    const [description, setDescription] = useState("")
    const [position, setPosition] = useState(null)
    const [errors, setErrors] = useState([])
    const [variant, setVariant] = useState("")
    const createStatus = useSelector(selectCreateStatus)
    //const [createStatus, setCreateStatus] = useState("")
    const dispatch = useDispatch()

    const validateInput = () => {
        if (name.length === 0 || name.length > 80) {
            return false
        }
        let now = dayjs().format("x")
        let twoYearsAhead =  dayjs().add(365 * 2, 'day')
        if (startDate === null || parseInt(startDate.format("x")) < now || parseInt(startDate.format("x")) > twoYearsAhead) {
            return false;
        }
        if (endDate === null || parseInt(endDate.format("x")) < now || parseInt(endDate.format("x")) > twoYearsAhead ||
            parseInt(endDate.format("x")) < parseInt(startDate.format("x"))) {
            return false
        }
        if (description.length > 1000) {
            return false
        }
        let attendeesNum = Number(attendees)
        if (isNaN(attendeesNum) || attendeesNum < 0 || !Number.isInteger(attendeesNum) || attendeesNum > 100) {
            return false
        }
        return true
    }

    const handleSubmit = () => {
        if (validateInput()) {
            dispatch(createPlan({
                name: name,
                description: description,
                position: position,
                startDate: parseInt(startDate.format("x")) / 1000,
                endDate: parseInt(endDate.format("x")) / 1000,
                attendees: attendees
            }))
        } else {
            dispatch(setCreateStatusInvalid())
        } /*else {
            let errors = []
            if (name.length === 0) {
                errors.push("Name is required")
            }
            if (position === null) {
                errors.push("Position is required")
            }
            if (startDate === null) {
                errors.push("Start date is required")
            }
            if (endDate === null) {
                errors.push("End date is required")
            }
            setErrors(errors)
            setVariant("danger")
        }*/
        /*if(validateInput() === false){
            setCreateStatus(400);
        } else {
            setCreateStatus(200);
        }*/
    }

    useEffect(() => {
        dayjs.extend(AdvancedFormat)
        if (createStatus) {
            return () => {
                dispatch(cleanCreateStatus())
            }
        }
    }, [dispatch])

    useEffect(() => {
        if (createStatus && createStatus === 200) {
            setErrors(["Plan successfully created"])
            setVariant("success")
        } else if (createStatus && createStatus >= 400) {
            setErrors(["Invalid input error"])
            setVariant("danger")
        } else if (createStatus && createStatus >= 500) {
            setErrors(["Internal server error"])
            setVariant("danger")
        }
    }, [createStatus])

    const listErrors = () => <div id='error-wrapper' className="error-wrapper">{errors.map((error) => {
        return <div>{error}</div>
    })}</div>

    return (
        <Container>
            <Col md={{span: 8, offset: 2}} lg={{span: 6, offset: 3}}
                 style={{justifyContent: "center", textAlign: "center"}}>
                <Navigation view={"planForm"}/>

                {localStorage.getItem("username") === null ?
                    <Alert variant="danger">
                        <Alert.Heading> You need to login first </Alert.Heading>
                    </Alert> :
                    <div><Form style={{textAlign: "left"}}>
                        <Form.Group controlId="formEventName">
                            <Form.Label style={{margin: "10px 0"}}>Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter event name"
                                          onChange={(e) => setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="formEventDescription">
                            <Form.Label style={{margin: "10px 0"}}>Description</Form.Label>
                            <Form.Control as="textarea" style={{resize: "none"}} rows={3}
                                          placeholder="Enter event description"
                                          onChange={(e) => setDescription(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="formEventAttendees">
                            <Form.Label style={{margin: "10px 0"}}>Number of Attendees</Form.Label>
                            <Form.Control type="text" placeholder="Enter number of attendees"
                                          onChange={(e) => setAttendees(e.target.value)}/>
                        </Form.Group>
                    </Form>
                        <div style={{margin: "10px 0", textAlign: "left"}}>Start Date</div>
                        <div id={'startDateWrapper'}>
                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                                <DateTimePicker
                                    renderInput={(props) => <TextField {...props} />}
                                    ampm={false}
                                    value={startDate}
                                    name={'startDateInput'}
                                    onChange={(newValue) => {
                                        setStartDate(newValue);
                                    }}
                                />
                            </LocalizationProvider>
                        </div>
                        <div style={{margin: "10px 0", textAlign: "left"}}>End Date</div>
                        <div id={'endDateWrapper'}>
                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                                <DateTimePicker
                                    renderInput={(props) => <TextField {...props} />}
                                    ampm={false}
                                    value={endDate}
                                    onChange={(newValue) => {
                                        setEndDate(newValue);
                                    }}
                                />
                            </LocalizationProvider>
                        </div>
                        <div style={{margin: "20px 0 0 0"}}>
                            <MapWrapper center={position} zoom={13} setPosition={setPosition} interactive={true}/>
                        </div>
                        {errors.length === 0 ? null :
                            <Alert variant={variant} onClose={() => {
                                setErrors([])
                                dispatch(cleanCreateStatus())
                            }} dismissible style={{margin: "20px 0 0 0"}}>
                                {listErrors()}
                            </Alert>
                        }
                        <Button onClick={handleSubmit} id={"submit"} style={{margin: "20px 0 20px 0"}}>Submit</Button>
                    </div>
                }
            </Col>
        </Container>
    )
}

export default PlanForm;