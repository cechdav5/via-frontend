import {
    createSlice,
    createAsyncThunk,
    current,
} from '@reduxjs/toolkit'

const initialState = {
    plans: [],
}

const backend = "http://localhost:5050"

export const fetchPlans = createAsyncThunk('plans/fetchPlans', async (_, thunkAPI) => {
    const authHeader = localStorage.getItem("authHeader") ? localStorage.getItem("authHeader") : ""
    return fetch(backend + "/plans/list", {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'authorization': authHeader
        },
    }).then(response => {
        if (response.status !== 200) {
            return thunkAPI.rejectWithValue(response.status)
        } else {
            return response.json()
        }
    })
})

export const deletePlan = createAsyncThunk('plans/deletePlan', async (id, thunkAPI) => {
    const authHeader = localStorage.getItem("authHeader") ? localStorage.getItem("authHeader") : ""
    return fetch(backend + "/plans/delete?id=" + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'authorization': authHeader
        },
    }).then(response => {
        if (response.status !== 200) {
            return thunkAPI.rejectWithValue(response.status)
        } else {
            return response.json()
        }
    })
})


export const createPlan = createAsyncThunk('plans/createPlan', async (data, thunkAPI) => {
    const authHeader = localStorage.getItem("authHeader") ? localStorage.getItem("authHeader") : ""
    return fetch(backend + "/plans/create", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'authorization': authHeader
        },
        body: JSON.stringify(data)
    }).then(response => {
        if (response.status !== 200) {
            return thunkAPI.rejectWithValue(response.status)
        } else {
            return response.json()
        }
    })
})

const plansSlice = createSlice({
    name: 'plans',
    initialState,
    reducers: {
        setCreateStatusInvalid(state) {
            return {...state, createStatus: 400}
        },
        cleanCreateStatus(state, _) {
            return {...state, createStatus: undefined}
        },
        cleanFetchAndDeleteStatus(state, _) {
            return {...state, fetchStatus: undefined, deleteStatus: undefined}
        }
    },
    extraReducers: builder => {
        builder.addCase(createPlan.fulfilled, (state, action) => {
            return {...state, plans: [...state.plans, action.payload], createStatus: 200}
        }).addCase(createPlan.rejected, (state, action) => {
            return {...state, createStatus: action.payload}
        }).addCase(deletePlan.fulfilled, (state, action) => {
            return {
                ...state, plans: current(state).plans.filter((item) => {
                    return item._id !== action.payload._id
                }), deleteStatus: 200
            }
        }).addCase(deletePlan.rejected, (state, action) => {
            return {...state, deleteStatus: action.payload}
        }).addCase(fetchPlans.fulfilled, (state, action) => {
            return {...state, plans: action.payload, fetchStatus: 200}
        }).addCase(fetchPlans.rejected, (state, action) => {
            return {...state, fetchStatus: action.payload}
        })
    }
})

export const {setCreateStatusInvalid, cleanCreateStatus, cleanFetchAndDeleteStatus} = plansSlice.actions

export default plansSlice.reducer