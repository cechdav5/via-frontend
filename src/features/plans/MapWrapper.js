import {MapContainer, Marker, Popup, TileLayer, useMapEvents} from "react-leaflet";
import React from "react";

function ClickListener(props) {
    const map = useMapEvents({
        click: (e) => {
            props.setPosition([e.latlng.lat, e.latlng.lng])
            map.flyTo(e.latlng)
        },
    })
    return null
}

function ChangeView({center}) {
    const map = useMapEvents({});
    if (center !== null) {
        if(map.getZoom()){
            map.setView(center, map.getZoom());
        } else {
            map.setView(center, 13);
        }
    } else {
        map.setView([50.08, 14.4], 13);
    }
    return null;
}

function MapWrapper(props) {
    return (
        <MapContainer scrollWheelZoom={false} click style={{width: "100%", height: "35vh", border: "solid 1px black"}}>
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {props.interactive ? <ClickListener setPosition={props.setPosition}/> : null}
            {props.center === null ? null : <Marker position={props.center} eventHandlers={{
                mouseover: (event) => event.target.openPopup(),
                mouseout: (event) => event.target.closePopup(),
            }}>
                <Popup>{Math.round(props.center[0] * 100000) / 100000 + " " + Math.round(props.center[1] * 100000) / 100000}</Popup>
            </Marker>}
            <ChangeView center={props.center} zoom={props.zoom}/>
        </MapContainer>
    )
}

export default MapWrapper;