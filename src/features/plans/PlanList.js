import {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux'
import dayjs from "dayjs";
import AdvancedFormat from "dayjs/plugin/advancedFormat";
import {cleanCreateStatus, cleanFetchAndDeleteStatus, deletePlan, fetchPlans} from "./plansSlice";
import {fetchUv, fetchWeather} from "../info/infoSlice";
import WeatherInfo from "../info/WeatherInfo";
import UvInfo from "../info/UvInfo";
import Button from 'react-bootstrap/Button';
import MapWrapper from "./MapWrapper";
import {Accordion, Container, Alert, Col} from "react-bootstrap";
import Navigation from "../accessories/Navigation";

const selectPlans = state => {
    let copy = [...state.plans.plans]
    copy.sort((a, b) => {
        return a.datetime - b.datetime
    })
    return {...state.plans, plans: copy}
}
const selectInfo = state => state.info

function PlanList() {
    const plans = useSelector(selectPlans)
    const info = useSelector(selectInfo)
    const [footerMode, setFooterMode] = useState(null)
    const [errorMessage, setErrorMessage] = useState(null)
    const [position, setPosition] = useState(null)
    const [variant, setVariant] = useState("danger")
    const dispatch = useDispatch()

    useEffect(() => {
        dayjs.extend(AdvancedFormat)
        dispatch(fetchPlans())
        return () => {
            dispatch(cleanFetchAndDeleteStatus())
        }
    }, [dispatch])

    useEffect(() => {
        if (plans.deleteStatus) {
            if (plans.deleteStatus === 200) {
                setErrorMessage("Deleted successfully")
                setVariant("success")
                setFooterMode("error")
            } else if (plans.deleteStatus >= 500) {
                setErrorMessage("Internal server error")
                setVariant("danger")
                setFooterMode("error")
            } else {
                setErrorMessage("Invalid request")
                setVariant("danger")
                setFooterMode("error")
            }
        }
    }, [plans.deleteStatus])

    const createPlanWrapper = (plan) => {
        return <Accordion.Item eventKey={plan._id}>
            <Accordion.Header>
                <div
                    style={{whiteSpace: "pre", wordWrap: "break-word"}}>{plan.name}</div>
            </Accordion.Header>
            <Accordion.Body style={{padding: "0"}}>
                <div>
                    <div style={{
                        textAlign: "justify",
                        padding: "15px 10px 0 15px",
                        color: "#0c63e4",
                        backgroundColor: "#e7f1ff"
                    }}>
                        {dayjs(plan.startDate * 1000).format("H:mm DD.MM.YYYY")} - {dayjs(plan.endDate * 1000).format("H:mm DD.MM.YYYY")}                    </div>
                    <div style={{
                        textAlign: "justify",
                        padding: "0 15px 10px 15px",
                        color: "#0c63e4",
                        backgroundColor: "#e7f1ff"
                    }}>
                        Attendees {plan.attendees}
                    </div>
                    <div style={{textAlign: "justify", margin: "25px 15px", wordWrap: "break-word"}}>
                        {plan.description}
                    </div>
                    <div style={{textAlign: "left"}}>
                        <Button onClick={() => {
                            handleWeather(plan)
                        }} style={{margin: "0 0 15px 10px"}}>Show weather</Button>
                        <Button onClick={() => {
                            handleUv(plan)
                        }} style={{margin: "0 0 15px 10px"}}>Show UV</Button>
                        <Button onClick={() => {
                            setPosition(plan.position)
                            setFooterMode("map")
                        }} style={{margin: "0 0 15px 10px"}}>Show location</Button>
                        <Button onClick={() => {
                            dispatch(deletePlan(plan._id))
                        }} style={{margin: "0 0 15px 10px"}}>Delete plan</Button>
                    </div>
                </div>
            </Accordion.Body>
        </Accordion.Item>
    }

    const dataAvailable = (datetime, numberOfDays) => {
        const diff = parseInt(datetime) * 1000 - parseInt(dayjs().format('x'))
        return diff >= 0 && diff < 86400000 * numberOfDays
    }

    const handleWeather = (plan) => {
        if (dataAvailable(plan.startDate, 5)) {
            dispatch(fetchWeather(plan))
            setFooterMode("weather")
        } else {
            setErrorMessage("Weather data unavailable for that date")
            setVariant("warning")
            setFooterMode("error")
        }
    }

    const handleUv = (plan) => {
        const start = plan.startDate - plan.startDate % 3600
        const end = start + 3600
        if (dataAvailable(plan.startDate, 5)) {
            dispatch(fetchUv({
                position: plan.position,
                start: dayjs(start * 1000).toISOString(),
                end: dayjs(end * 1000).toISOString()
            }))
            setFooterMode("uv")
        } else {
            setErrorMessage("UV data unavailable for that date")
            setVariant("warning")
            setFooterMode("error")
        }
    }

    const listPlans = (plans) => <Accordion>{plans.plans.map((plan) => createPlanWrapper(plan))}</Accordion>

    return <Container style={{textAlign: "center"}}>
        <Col lg={{span: 6, offset: 3}} style={{display: "flex", flexDirection: "column"}}>
            <Navigation view={"planList"}/>

            {plans.plans.length !== 0 && plans.fetchStatus === 200 ?
                <div style={{maxHeight: "50vh", overflowY: "auto"}}>
                    {listPlans(plans)}
                </div> : null
            }

            {plans.plans.length === 0 && plans.fetchStatus === 200 ?
                <div id={'no-plans-wrapper'}>
                    <Alert variant="warning">
                        <Alert.Heading> You have no plans </Alert.Heading>
                    </Alert>
                </div> : null
            }

            {plans.fetchStatus !== 200 ?
                <Alert variant="danger">
                    <Alert.Heading>{plans.fetchStatus === 500 ? "Internal server error" : "You need to login first"}</Alert.Heading>
                </Alert> : null
            }

            <div style={{flexGrow: 6}}>
                {footerMode === "map" ?
                    <div id={"map-container"}><MapWrapper interactive={false} center={position} zoom={13}/>
                    </div> : null}
                {footerMode === "weather" ? <WeatherInfo/> : null}
                {footerMode === "uv" ? <UvInfo/> : null}
                {footerMode === "error" ?
                    <div id={'error-wrapper'}><Alert variant={variant} onClose={() => setFooterMode(null)} dismissible>
                        <Alert.Heading>{errorMessage}</Alert.Heading>
                    </Alert></div> : null}
            </div>
        </Col>
    </Container>
}

export default PlanList