import {Link} from "react-router-dom";
import "./../../styles/navigation.css";

function Navigation(props) {
    return <div className="navigation" style={{display: "flex", flexDirection: "row", margin: "15px 0 15px 0"}}>
        {props.view === "planList" ?
            <div id={'list-nav-item'} className="active-navitem">Plan
                list</div> : <Link to="/" style={{textDecoration: "none"}}>
                <div id={'list-nav-item'} className="inactive-navitem">Plan list</div>
            </Link>}
        {props.view === "planForm" ? <div id={'create-nav-item'} className="active-navitem">New plan</div> :
            <Link to="/create" style={{textDecoration: "none"}}>
                <div id={'create-nav-item'} className="inactive-navitem">New plan</div>
            </Link>}
        {props.view === "login" ? <div id={'login-nav-item'} className="active-navitem">Login</div> :
            <Link to="/login" style={{textDecoration: "none"}}>
                <div id={'login-nav-item'} className="inactive-navitem">Login</div>
            </Link>}
        {props.view === "register" ? <div id={'register-nav-item'} className="active-navitem">Register</div> :
            <Link to="/register" style={{textDecoration: "none"}}>
                <div id={'register-nav-item'} className="inactive-navitem"> Register</div>
            </Link>}
    </div>
}

export default Navigation