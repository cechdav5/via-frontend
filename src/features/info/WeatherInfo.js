import {useSelector} from 'react-redux'
import {Row, Col} from "react-bootstrap";

const selectWeather = state => state.info.weather

function WeatherInfo() {
    const weather = useSelector(selectWeather)

    const capitalize = (words) => {
        return words.split(" ").map(word => {
            return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase() + " ";
        })
    }

    const ret = weather === null ? <div/> : <div id={"weather-container"}><Row style={{backgroundColor: "#e7f1ff", margin:"0", padding:"0 0 0.5rem 0", borderStyle:"solid", borderWidth:"1px", borderColor:"#dee2e6"}}>
        <Col style={{height:"250px", display: "flex", flexDirection: "column"}} xs={12} sm={6}>
            <div style={{flexGrow: 1}}>
                <img alt="weather-icon" src={"http://openweathermap.org/img/wn/" + weather.weather[0].icon + "@4x.png"}/>
            </div>
            <div style={{display: "flex", alignContent:"center", justifyContent:"center", flexDirection:"column", flexGrow: 3}}>{capitalize(weather.weather[0].description)}</div>
        </Col>
        <Col style={{display: "flex", flexDirection: "column"}} xs={12} sm={6}>
            <div style={{margin: "0.5rem 0 0 0", display: "flex", alignContent:"center", justifyContent:"center", flexDirection:"column", flexGrow: 1}}>Cloudiness {weather.clouds.all}%</div>
            <div style={{margin: "0.5rem 0 0 0", display: "flex", alignContent:"center", justifyContent:"center", flexDirection:"column", flexGrow: 1}}>Probability of precipitation {weather.pop}%</div>
            <div style={{margin: "0.5rem 0 0 0", display: "flex", alignContent:"center", justifyContent:"center", flexDirection:"column", flexGrow: 1}}>Wind speed {weather.wind.speed} m/s</div>
            <div style={{margin: "0.5rem 0 0 0", display: "flex", alignContent:"center", justifyContent:"center", flexDirection:"column", flexGrow: 1}}>Temperature {weather.main.temp} ℃</div>
        </Col>
    </Row></div>
    return ret
}

export default WeatherInfo