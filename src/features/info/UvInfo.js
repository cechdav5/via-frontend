import {useSelector} from "react-redux";
import {useEffect, useState} from 'react';
import '../../styles/uvinfo.css';

const uvSelector = (state) => state.info.uv

function UvInfo() {
    const uv = useSelector(uvSelector)
    const [color, setColor] = useState(null);
    const [borderColor, setBorderColor] = useState(null);

    useEffect(() => {
        if (uv < 3) {
            setColor("green")
            setBorderColor("#006600")
        } else if (uv < 6) {
            setColor("yellow")
            setBorderColor("#CCCC00")
        } else if (uv < 8) {
            setColor("orange")
            setBorderColor("#CC8400")
        } else if (uv < 11) {
            setColor("red")
            setBorderColor("#CC0000")
        } else {
            setColor("purple")
            setBorderColor("#660066")
        }
    }, [uv]);

    const scale = () => {
        let ret = []
        for (let i = 1; i < 12; i++) {
            if (i <= uv) {
                ret.push(<div className="uv-item" style={{backgroundColor: color, borderColor: borderColor}}>{i}</div>)
            } else {
                ret.push(<div className="uv-item" style={{backgroundColor: "#e7f1ff"}}>{i}</div>)
            }
        }
        return ret
    }

    const ret = uv === null ? <div/> : <div className="uv-container" id={"uv-container"}>
        <div className="uv-title">UV index: {uv}</div>
        {scale()}
    </div>
    return ret
}

export default UvInfo