import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

const initialState = {
    weather: null,
    uv: null
}

const selectWeatherInfo = (plan, data) => {
    let ret = null
    let min = 1e14
    for (let i = 0; i < data.list.length; i++) {
        let d = data.list[i]
        if (Math.abs(d.dt - plan.startDate) < min) {
            min = Math.abs(d.dt - plan.startDate)
            ret = d
        }
    }
    return ret
}

export const fetchWeather = createAsyncThunk('info/fetchWeather', async (plan) => {
    const res = await fetch("https://api.openweathermap.org/data/2.5/forecast?lat=" + plan.position[0] + "&lon=" + plan.position[1] + "&appid=6ff07baf861431039b4eed5111f8609c&units=metric", {
        method: 'GET',
        headers: {
            accept: 'application/json'
        }
    })
    return res.json()
})

export const fetchUv = createAsyncThunk('info/fetchUv', async (params) => {
    console.log(params)
    const res = await fetch("https://data.climacell.co/v4/timelines?apikey=HTR1UThGR8jyTQEubdQaofRUEhRPYgBU&location=" + params.position[0] + "," + params.position[1] + "&fields=uvHealthConcern&startTime=" + params.start + "&endTime=" + params.end, {
        method: 'GET',
        headers: {
            accept: 'application/json'
        }
    })
    return res.json()
})

const infoSlice = createSlice({
    name: 'info',
    initialState,
    extraReducers: builder => {
        builder.addCase(fetchWeather.fulfilled, (state, action) => {
            console.log(action.payload)
            const w = selectWeatherInfo(action.meta.arg, action.payload)
            console.log(w)
            return {...state, weather: w}
        }).addCase(fetchUv.fulfilled, (state, action) => {
            console.log(action.payload)
            return {...state, uv: action.payload.data.timelines[0].intervals[0].values.uvHealthConcern}
        })
    }
})

export default infoSlice.reducer