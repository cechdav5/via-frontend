import {configureStore} from '@reduxjs/toolkit'

import plansReducer from './features/plans/plansSlice'
import infoReducer from './features/info/infoSlice'
import authReducer from './features/auth/authSlice'

const store = configureStore({
    reducer: {
        plans: plansReducer,
        info: infoReducer,
        auth: authReducer
    },
})

export default store
