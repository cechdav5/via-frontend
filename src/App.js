import React from 'react';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import PlanForm from "./features/plans/PlanForm";
import PlanList from "./features/plans/PlanList";
import Login from "./features/auth/Login";
import Register from "./features/auth/Register";
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<PlanList/>}/>
                    <Route path="/login" element={<Login/>}/>
                    <Route path="/register" element={<Register/>}/>
                    <Route path="/create" element={<PlanForm/>}/>
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
