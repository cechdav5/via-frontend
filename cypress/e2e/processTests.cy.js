let formatInputDate = (d) => {
    return "".concat(String(d.getMonth() + 1).padStart(2, '0'), String(d.getDate()).padStart(2, '0'),
        d.getFullYear(), String(d.getHours()).padStart(2, '0'), String(d.getMinutes()).padStart(2, '0'))
}

let formatShowedDate = (d) => {
    return "".concat(String(d.getHours()).padStart(2, '0'), ':', String(d.getMinutes()).padStart(2, '0'), ' ',
        String(d.getDate()).padStart(2, '0'), '.', String(d.getMonth() + 1).padStart(2, '0'), '.', d.getFullYear())
}

describe('Process tests', () => {
    it(`Process: successful register, repeated login, create a plan, view plan, delete created plan`, () => {
        cy.visit("http://localhost:3030/register")
        cy.get('#formUsername').type('test12467610121416')
        cy.get('#formEmail').type('test12467610121416@gmail.com')
        cy.get('#formPassword').type('test')
        cy.get('#formRepassword').type('test')
        cy.get('#root > div > div > div > button').click()
        cy.get('#error-wrapper').should('contain', 'Registered successfully')

        cy.get('#login-nav-item').click()
        cy.get('#formEmail').type('test12467610121416@gmail.com')
        cy.get('#formPassword').type('wrongPassword')
        cy.get('#root > div > div > div > button').click()
        cy.get('#error-wrapper').should('contain', 'Invalid credentials')

        cy.get('#formPassword').clear().type('test')
        cy.get('#root > div > div > div > button').click()
        cy.get('#logged-in-as').should('be.visible')

        cy.get('#create-nav-item').click()
        cy.get('#formEventName').type('test')
        cy.get('#formEventAttendees').type('5')
        let startDate = new Date()
        startDate.setDate(startDate.getDate() + 1)
        cy.get('#startDateWrapper input').type(formatInputDate(startDate))
        let endDate = new Date(startDate.getTime())
        endDate.setDate(endDate.getDate() + 1)
        cy.get('#endDateWrapper input').type(formatInputDate(endDate))
        cy.get('#root > div > div > div > div:nth-child(2) > div:nth-child(6) > div').click(200, 80)
        cy.get('#root > div > div > div > div:nth-child(2) > button').click()
        cy.get('#error-wrapper').should('contain', 'Plan successfully created')
        cy.get('#list-nav-item').click()

        cy.get('#root > div > div > div > div:nth-child(2) > div > div > h2').click()
        cy.get('#root > div > div > div > div:nth-child(2) > div > div > h2').should('contain', 'test')
        cy.get('#root > div > div > div > div:nth-child(2) > div > div > div > div > div > div:nth-child(1)').should('contain', formatShowedDate(startDate) + ' - ' + formatShowedDate(endDate))
        cy.get('#root > div > div > div > div:nth-child(2) > div > div > div > div > div > div:nth-child(2)').should('contain', 'Attendees 5')
        cy.get('#root > div > div > div > div:nth-child(2) > div > div > div > div > div > div:nth-child(4) > button:nth-child(4)').click()
        cy.get('#error-wrapper').should('contain', 'Deleted successfully')
    })

    it(`Process: Repeated register, login, view plans`, () => {
        cy.visit("http://localhost:3030/register")
        cy.get('#formUsername').type('test12467610121416')
        cy.get('#formEmail').type('test12467610121416@gmail.com')
        cy.get('#formPassword').type('test')
        cy.get('#formRepassword').type('test')
        cy.get('#root > div > div > div > button').click()
        cy.get('#error-wrapper').should('contain', 'User with such email does already exist')

        cy.get('#formUsername').clear().type('test12894611')
        cy.get('#formEmail').clear().type('test12894611@gmail.com')
        cy.get('#root > div > div > div > button').click()
        cy.get('#error-wrapper').should('contain', 'Registered successfully')

        cy.get('#login-nav-item').click()
        cy.get('#formEmail').type('test12894611@gmail.com')
        cy.get('#formPassword').type('test')
        cy.get('#root > div > div > div > button').click()
        cy.get('#logged-in-as').should('be.visible')

        cy.get('#list-nav-item').click()
        cy.get('#no-plans-wrapper').should('be.visible')
    })

    it(`Process: Successful login, repeated create plan, view plans, delete a plan`, () => {
        cy.visit("http://localhost:3030/login")
        cy.get('#formEmail').type('test12894611@gmail.com')
        cy.get('#formPassword').type('test')
        cy.get('#root > div > div > div > button').click()
        cy.get('#logged-in-as').should('be.visible')

        cy.get('#create-nav-item').click()
        cy.get('#formEventName').type('foo')
        cy.get('#formEventAttendees').type('5')
        let startDate = new Date()
        startDate.setDate(startDate.getDate() + 1)
        cy.get('#startDateWrapper input').type(formatInputDate(startDate))
        let endDate = new Date(startDate.getTime())
        endDate.setDate(endDate.getDate() + 1)
        cy.get('#endDateWrapper input').type(formatInputDate(endDate))
        cy.get('#root > div > div > div > div:nth-child(2) > div:nth-child(6) > div').click(200, 80)
        cy.get('#root > div > div > div > div:nth-child(2) > button').click()
        cy.get('#error-wrapper').should('contain', 'Plan successfully created')
        cy.get('#root > div > div > div > div:nth-child(2) > div.fade.alert.alert-success.alert-dismissible.show > button').click()

        cy.get('#formEventName').clear().type('bar')
        cy.get('#formEventAttendees').clear().type('8')
        startDate.setDate(startDate.getDate() + 1)
        cy.get('#startDateWrapper input').clear().type(formatInputDate(startDate))
        endDate.setDate(endDate.getDate() + 1)
        cy.get('#endDateWrapper input').clear().type(formatInputDate(endDate))
        cy.get('#root > div > div > div > div:nth-child(2) > div:nth-child(6) > div').click(200, 80)
        cy.get('#root > div > div > div > div:nth-child(2) > button').click()
        cy.get('#error-wrapper').should('contain', 'Plan successfully created')

        cy.get('#list-nav-item').click()
        cy.get('#root > div > div > div > div:nth-child(2) > div > div:nth-child(1) > h2').should('contain', 'foo')
        cy.get('#root > div > div > div > div:nth-child(2) > div > div:nth-child(2) > h2').should('contain', 'bar')
        cy.get('#root > div > div > div > div:nth-child(2) > div > div:nth-child(1) > h2').click()
        cy.get('#root > div > div > div > div:nth-child(2) > div > div:nth-child(1) > div > div > div > div:nth-child(4) > button:nth-child(4)').click()
        cy.get('#error-wrapper').should('contain', 'Deleted successfully')
        cy.get('#root > div > div > div > div:nth-child(2) > div > div:nth-child(1) > h2').should('contain', 'bar')
    })
})
