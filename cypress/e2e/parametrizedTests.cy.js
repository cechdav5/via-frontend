let textProvider = (str, charLimit) => {
    //console.log(str)
    if (str === 'null') {
        //console.log('returning null')
        return null
    } else if (str === 'tooLong') {
        //console.log('returning too long')
        return 'a'.repeat(charLimit + 1)
    } else if (str === 'validString') {
        //console.log('returning valid')
        return 'b'.repeat(charLimit)
    } else {
        return str
    }
}

let dateProvider = (str, futureDaysLimit, startDayOffset, endDayOffset) => {
    let formatDate = (d) => {
        return "".concat(String(d.getMonth() + 1).padStart(2, '0'), String(d.getDate()).padStart(2, '0'), '',
            d.getFullYear(), '', String(d.getHours()).padStart(2, '0'), '', String(d.getMinutes()).padStart(2, '0'))
    }
    if (str === 'null') {
        return null
    } else if (str === 'tooAhead') {
        let d = new Date()
        let tmp = d.getDate()
        d.setDate(tmp + futureDaysLimit + 1)
        return formatDate(d)
    } else if (str === 'past') {
        let d = new Date()
        let tmp = d.getDate()
        d.setDate(tmp - 1)
        return formatDate(d)
    } else if (str === 'lessThanStart') {
        let d = new Date()
        let tmp = d.getDate()
        d.setDate(tmp + startDayOffset - 1)
        return formatDate(d)
    } else {
        let d = new Date()
        let tmp = d.getDate()
        d.setDate(tmp + endDayOffset)
        return formatDate(d)
    }
}

const twoWayInput = require('../resources/2-way-sem-output.csv')

const mixedStrengthInput = require('../resources/mixed-strength-sem-output-complete.csv')


describe('Parametrized tests', () => {
    before(() => {
        cy.visit("http://localhost:3030/register")
        cy.get('#formUsername').type('test')
        cy.get('#formEmail').type('test@gmail.com')
        cy.get('#formPassword').type('test')
        cy.get('#formRepassword').type('test')
        cy.get('#root > div > div > div > button').click()
        cy.get('#error-wrapper').invoke('text').then((text) => {
            if (text === "Registered successfully") {
                console.log("Registered successfully")
            } else if (text === "User with such email does already exist") {
                console.log("User with such email does already exist")
            }
            if (text !== "User with such email does already exist" &&
                text !== "Registered successfully") {
                throw new Error("Invalid registration outcome")
            }
        })
    })

    beforeEach(() => {
        cy.visit("http://localhost:3030/login")
        cy.get('#formEmail').type('test@gmail.com')
        cy.get('#formPassword').type('test')
        cy.get('#root > div > div > div > button').click()
        cy.get('#logged-in-as').should('be.visible')
        cy.get('#root > div > div > div > div.navigation > a:nth-child(2) > div').click()
    })

    twoWayInput.forEach(values => {
        it(`2-way: Creating plan for title ${values[1]}, description ${values[2]}, attendees ${values[0]}, startDate ${values[3]}, endDate ${values[4]}`, () => {
            //console.log(values)
            //console.log(values[1])
            attendees = textProvider(values[0], -1)
            title = textProvider(values[1], 80)
            //console.log(title)
            description = textProvider(values[2], 1000)
            startDate = dateProvider(values[3], 365 * 2, 7, 7)
            endDate = dateProvider(values[4], 365 * 2, 7, 8)

            if (title !== null) {
                cy.get('#formEventName').invoke('val', title).type('a{backspace}')
                //cy.get('#formEventName').type(title)
            }
            if (description !== null) {
                cy.get('#formEventDescription').invoke('val', description).type('a{backspace}')
            }
            if (attendees !== null) {
                cy.get('#formEventAttendees').type(attendees, {delay: 0.01})
            }
            if (startDate !== null) {
                cy.get('#startDateWrapper input').type(startDate, {delay: 0.01})
            }
            if (endDate !== null) {
                cy.get('#endDateWrapper input').type(endDate, {delay: 0.01})
            }
            cy.get('#root > div > div > div > div:nth-child(2) > div:nth-child(6) > div').click(200, 80)
            cy.get('#root > div > div > div > div:nth-child(2) > button').click()
            if (values[5] === 'ok') {
                cy.get('#error-wrapper').should('contain', 'Plan successfully created')
            } else {
                cy.get('#error-wrapper').should('contain', 'Invalid input error')
            }
        })
    })

    mixedStrengthInput.forEach(values => {
        it(`Mixed-strength: Creating plan for title ${values[2]}, description ${values[3]}, attendees ${values[4]}, startDate ${values[0]}, endDate ${values[1]}`, () => {
            attendees = textProvider(values[4], -1)
            title = textProvider(values[2], 80)
            description = textProvider(values[3], 1000)
            startDate = dateProvider(values[0], 365 * 2, 7, 7)
            endDate = dateProvider(values[1], 365 * 2, 7, 8)
            if (title !== null) {
                cy.get('#formEventName').invoke('val', title).type('a{backspace}')
            }
            if (description !== null) {
                cy.get('#formEventDescription').invoke('val', description).type('a{backspace}')
            }
            if (attendees !== null) {
                cy.get('#formEventAttendees').type(attendees, {delay: 0.01})
            }
            if (startDate !== null) {
                cy.get('#startDateWrapper input').type(startDate, {delay: 0.01})
            }
            if (endDate !== null) {
                cy.get('#endDateWrapper input').type(endDate, {delay: 0.01})
            }
            cy.get('#root > div > div > div > div:nth-child(2) > div:nth-child(6) > div').click(200, 80)
            cy.get('#root > div > div > div > div:nth-child(2) > button').click()
            if (values[5] === 'ok') {
                cy.get('#error-wrapper').should('contain', 'Plan successfully created')
            } else {
                cy.get('#error-wrapper').should('contain', 'Invalid input error')
            }
        })
    })
})
