FROM node:18.12.1-alpine

WORKDIR /usr/frontend/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3030
CMD ["npm", "start"]