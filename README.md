# VIA - Event Planner Frontend

Cílem projektu je vytvořit jednoduchý plánovač, ve kterém má uživatel 
možnost vytvářet plány s popisem, datem a místem konání (vlastní 
API). Lokality těchto plánů si poté může zobrazit na mapě a také si u 
nich zobrazit předpověď počasí (public API 1) a přepokládaný UV index 
(public API 2) s určitým předstihem (horizont několika dnů). Projekt je vyvíjen
ve frameworku React.

Repozitář serveru, který poskytuje vlastní API dostupný [zde](https://gitlab.fel.cvut.cz/cechdav5/via-backend).
Blog aplikace dostupný [zde](https://gitlab.fel.cvut.cz/cechdav5/via-frontend/-/wikis/Progress-information).

## Dostupné skripty

V adresáři projektu lze spustit následující příkazy:

### `npm start`

Spustí aplikaci v development mode na adrese [http://localhost:3030](http://localhost:3000).

Stránka se automaticky načte znovu při změně kódu.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.